from setuptools import setup

setup(
    name="django-hotsauce",
	version="0.1.0",
	description="",
	author="Eric Satterwhite",
	author_email="eric@codedependant.net",
	packages=[ "hotsauce" ],
	install_requires=[ "diff-match-patch==20110725.1"],
	classifiers=[
		"Framework::Django",
		"Programming Language::Python2.7"
	]
)
