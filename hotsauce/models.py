''' hotsauce Models '''
from django.conf import settings
from django.db import models
from diff_match_patch import diff_match_patch
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
#from django.contrib import contenttypes 
from datetime import datetime
from django.db.models import Manager
from django_extensions.db.fields import AutoSlugField

#diff_match_patch instance
DMP = diff_match_patch()
class ChangeSet( models.Model ):
    ''' 
        The ChangeSet can be attached to any object and
        
        ment to be connected to a Class which subclases
        EditableItem as a generic inline relation
        
        import this in to your application's admin.py
        create a generic inlin and attach it to anything
        which subclasses EditableItem as a generic inline
        and you have a "wiki-able" item
    '''
    content_type = models.ForeignKey( ContentType )
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey( 'content_type', 'object_id' )
    editor = models.ForeignKey( settings.AUTH_USER_MODEL, limit_choices_to={'is_staff':True} )
    revision = models.PositiveIntegerField()
    comment = models.CharField( max_length=255, blank=True )
    content_diff = models.TextField( 'Content Patch', editable=False, blank=True )
    date_modified = models.DateTimeField( default=datetime.now(), blank=False )
    old_title = models.CharField( "Title", max_length=255, blank=True )

    objects = Manager()

    class Meta:
        ordering = ( '-revision', )
        get_latest_by = 'date_modified'

    def __unicode__( self ):
        return "revision %s" % self.revision

    def reapply( self, editor ):
        ''' 
            apply the patch to the content_object
            which will always be an EditableItem
        '''
        #get content object
        editable_object = self.content_object

        #get all changesets greater than self.revision ordered -revision
        changesets = editable_object.changes.filter( 
                                                      revision__gt=self.revision
                                                      ).order_by( '-revision' )

        #collect the content diff & convert to patch
        content = None
        for change in changesets:
            if content is None:
                content = editable_object.get_html_content()

            patch = DMP.patch_fromText( change.content_diff )

            #apply patches to the current content object
            content = DMP.patch_apply( patch, content )[0]
            change.reverted = True
            change.save()

        old_content = editable_object.get_html_content()
        old_title = editable_object.title

        editable_object.content = content
        editable_object.save()

        editable_object.make_new_revision( old_content,
                                          old_title,
                                          comment="Reverted to revision %s" % self.revision,
                                          editor=editor )
    def display_change_html( self ):
        ''' return the html string of a changeset'''
        old_content = self.content_object.get_html_content()
        newer_changesets = ChangeSet.objects.filter( content_type=self.content_type,
                                                    object_id=self.content_object.id,
                                                    revision__gte=self.revision )
        for i, changeset in enumerate( newer_changesets ):
            patches = DMP.patch_fromText( changeset.content_diff )

            if len( newer_changesets ) == i + 1:
                next_rev_content = old_content
            old_content = DMP.patch_apply( patches, old_content )[0]

        diffs = DMP.diff_main( old_content, next_rev_content, checklines=False )
        return DMP.diff_prettyHtml( diffs )
    def see_item_at_version( self ):
        ''' reverts the content of the object and returns it with out saving'''
        editable_object = self.content_object
        #get all changesets greater than self.revision ordered -revision
#        if version == 1:
#            changesets = editable_object.changes.filter(
#                                                      revision__gte=self.revision
#                                                      ).order_by('-revision')
#        else:
        changesets = editable_object.changes.filter( 
                                                      revision__gte=self.revision
                                                      ).order_by( '-revision' )

        #collect the content diff & convert to patch
        content = None
        for change in changesets:
            if content is None:
                content = editable_object.get_html_content()
            patch = DMP.patch_fromText( change.content_diff )
            #apply patches to the current content object
            content = DMP.patch_apply( patch, content )[0]
        return content


    def display_diff_html( self ):
        # get the content object's HTML
        # get all of its changesets greater than this version number
        # aplly the patches ( do not save over )
        # get difference HTML between the reverted object ( if reverted )
        # return the content object HTML( original )
        # return the difference HTML
        document_obj = self.content_object
        #get all changesets greater than self.revision ordered -revision
        changesets = document_obj.changes.filter( revision__gt=self.revision
                                                 ).order_by( '-revision' ) or None

        #collect the content diff & convert to patch
        current_html = None
        if changesets is not None:
            for change in changesets:
                if current_html is None:
                    current_html = document_obj.get_html_content()

                patch = DMP.patch_fromText( change.content_diff )

                #apply patches to the current content object
                current_html = DMP.patch_apply( patch, current_html )[0]
        else:
            current_html = document_obj.get_html_content()


        diffs = DMP.diff_main( current_html, document_obj.get_html_content(), checklines=False )
        return DMP.diff_prettyHtml( diffs )

class EditableItem( models.Model ):
    ''' 
        base item for wiki. only field is main editable content
        we want to version
        
        This can be imported into any application that wishes
        to maintain a version log
    '''
    title = models.CharField( 'Title', max_length=255 )
    slug = AutoSlugField( populate_from=( 'title' ), unique=True, help_text="A Slug is a URL friendly phrase for identifying objects.\
                                                This will be formulated for you", db_index=True )
    content = models.TextField( 'Editable Content' )
    editor = models.ForeignKey( settings.AUTH_USER_MODEL )
    date_created = models.DateTimeField( 'Creation Date', default=datetime.now )
    date_modified = models.DateTimeField( 
                                         default=datetime.now,
                                         auto_now=True,
                                         editable=False
                                       )
    class Meta:
        abstract = True

    def revert_to( self, revision, editor=None ):
        '''takes a revision number queries for the changset and returns it '''

        #a ChangeSet Instance
        changeset = self.changeset_set.objects.get( version=revision )
        changeset.reapply( editor )

    def make_new_revision( self, old_content, old_title, comment, editor ):
        '''
            Function to be overridden
            
            This is the function that is called from the form that 
            is responsible for saving and editable item.
            
            This creates a new ChangeSet related to the object that called
            this function
        '''
        from hotsauce.utils import make_difPatch
        diff_text = make_difPatch( self.content, old_content )
        change = ChangeSet.objects.create( content_diff=diff_text,
                                      content_type=self.content_type,
                                      object_id=self.id,
                                      comment=comment,
                                      old_title=old_title,
                                      editor=editor )
        return change

    def get_ct( self ):
        ''' returns the ID of this objects ContentType'''
        return ContentType.objects.get_for_model( self )

class WikiPage( EditableItem ):
    '''DOCSTRINGS'''
    changes = generic.GenericRelation( ChangeSet )
    objects = Manager()
    def __unicode__( self ):
        return "%s" % self.slug
    def __str__( self ):
        return self.__unicode__()
    #in the case we want to revert back to the old content
    def make_new_revision( self, old_content,
                          old_title, comment, editor ):
        '''Overridden from EditableItem'''
        ctype = ContentType.objects.get_for_model( self )
        from hotsauce.utils import make_difPatch
        diff_text = make_difPatch( self.content, old_content )
        cset = ChangeSet.objects.create( content_diff=diff_text,
                                      content_type=ctype,
                                      object_id=self.id,
                                      comment=comment,
                                      editor=editor,
                                      old_title=old_title )
        return cset

